﻿using UnityEngine;
using UnityEngine.UI;

public class TestMatchMaker : Photon.MonoBehaviour
{
	private PhotonView myPhotonView;
	public Text numberOfPlayersText;
	GameObject firstPerson ;

	void Start()
	{
		PhotonNetwork.ConnectUsingSettings("0.1");
	}
	
	void OnJoinedLobby()
	{
		Debug.Log("JoinRandom");
		PhotonNetwork.JoinRandomRoom();
	}
	
	void OnPhotonRandomJoinFailed()
	{
		Debug.Log("JoinRandomFailed");
		PhotonNetwork.CreateRoom (null,true,true,2);

	}
	
	void OnJoinedRoom()
	{
   		if(PhotonNetwork.room.playerCount==1)
			numberOfPlayersText.text = PhotonNetwork.room.playerCount.ToString();
		//PhotonNetwork.Instantiate("object", Vector3.zero, Quaternion.identity, 0);
	}


	
	void OnGUI()
	{
		GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());

	}
}
