﻿using UnityEngine;
using System.Collections;
using Leap;

public class PoseManager {

	public enum POSE {ROCK,PAPER,SCISSOR,NULL}; 
	public enum RESULT{ WIN,LOSE,TIE,INCORRECT_POSE};

	

	public POSE detectGesture(Hand hand){

		if (hand.SphereRadius > 90) {
			return POSE.PAPER;
		} else if (hand.Fingers [1].IsExtended 
		           && hand.Fingers [2].IsExtended
		           && !hand.Fingers [0].IsExtended 
		           && !hand.Fingers [4].IsExtended
		           && !hand.Fingers [5].IsExtended) {
			return POSE.SCISSOR;
		} else if (hand.SphereRadius < 40) {
			return POSE.ROCK;
		} else {
			return POSE.NULL;
		}

	}

	public RESULT didIWin(string myPOSE,string otherPOSE){

		Debug.Log ("my pose" + myPOSE + " other Pose" + otherPOSE);
		
		if (myPOSE == "NULL" || otherPOSE == "NULL")
			return RESULT.INCORRECT_POSE;

		if(myPOSE==otherPOSE)
			return RESULT.TIE;

		if(myPOSE=="ROCK" && otherPOSE=="SCISSOR")
			return RESULT.WIN;

		if(myPOSE=="PAPER" && otherPOSE=="ROCK")
			return RESULT.WIN;

		if(myPOSE=="SCISSOR" && otherPOSE=="PAPER")
			return RESULT.WIN;

		return RESULT.LOSE;



	}
}



