﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Leap;

public class NetworkCharacters : Photon.MonoBehaviour{

	private Vector3 correctPlayerPos = Vector3.zero; // We lerp towards this
	private Quaternion correctPlayerRot = Quaternion.identity; // We lerp towards this

	static string HAND_PREFAB_NAME="ImageFullLeftHand(Clone)";
	GameObject leftHandModel ;//= GameObject.Find("SecondPlayerLeft");
	GameObject rightHandModel ;//= GameObject.Find("SecondPlayerRight");
	PoseManager poseManager ;//= new PoseManager ();
	HandController controller;
	Text result;
	string pose;
	bool go =false;
	bool IAmReady=false;
	bool stop = false;

	float time=3f;
	Frame f;

	void Start(){

		controller = GameObject.Find("HeadMountedHandController").GetComponent<HandController>();
		leftHandModel = GameObject.Find("SecondPlayerLeft");
		rightHandModel = GameObject.Find("SecondPlayerRight");
		result = GameObject.Find ("ResultText").GetComponent<Text>();
		poseManager=new PoseManager();
		pose="NULL";

	}

	void Update(){
		if (!photonView.isMine){
			//            transform.position = Vector3.Lerp(transform.position, this.correctPlayerPos, Time.deltaTime * 5);
			//            transform.rotation = Quaternion.Lerp(transform.rotation, this.correctPlayerRot, Time.deltaTime * 5);
		}

		f = controller.GetFrame ();
		if (stop)
			return;
		if (!IAmReady) {
			result.text="Make a fist to get Ready";
			if (poseManager.detectGesture (f.Hands [0]) == PoseManager.POSE.ROCK)
				IAmReady = true;
		} else if(go){
			time = time - Time.deltaTime;
			if(time>0)
				result.text = time.ToString();
			else{
				stop=true;
				Debug.Log (stop);
			}

		}
			

	}

	LeapHandWrapper getHand(){

		if (f.Hands.Count != 1) {
			return null;
		}


		if (f.Hands [0].IsLeft) {
			return  new LeapHandWrapper( GameObject.Find ("ImageFullLeftHand(Clone)"),"Left",poseManager.detectGesture(f.Hands[0]) );
		}
		
		if (f.Hands [0].IsRight) {
			return  new LeapHandWrapper( GameObject.Find ("ImageFullRightHand(Clone)"),"Right",poseManager.detectGesture(f.Hands[0]) );
		}
		
		return null;
	}



	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info){
		
		if (stream.isWriting) {
			
			LeapHandWrapper realHand = getHand(); 

			if (realHand == null){

				return;
			}

			Transform realHandContainer = realHand.hand.transform.FindChild ("HandContainer");

			stream.SendNext (realHand.orientation);
			stream.SendNext (realHand.pose.ToString());

			stream.SendNext (realHandContainer.transform.rotation);

            for (int i = 0; i< realHandContainer.GetChild(0).childCount; i++) {

                Quaternion[] arr = new Quaternion[3];
                Transform finger = realHandContainer.GetChild (0).GetChild (i);

                Transform bone1 = finger.GetChild (0);
                Transform bone2 = bone1.GetChild (0);
                Transform bone3 = bone2.GetChild (0);

                arr [0] = bone1.rotation;
                arr [1] = bone2.rotation;
                arr [2] = bone3.rotation;
                stream.SendNext (arr);

            }
			stream.SendNext(IAmReady);
		
			
		} else {
			//Debug.Log ("what is love");

			string handOrientation=(string)stream.ReceiveNext (); //left or right

			string otherPose=(string)stream.ReceiveNext();

			Frame f = controller.GetFrame ();

			if (f.Hands.Count != 1) {
				return ;
			}

			string pose=poseManager.detectGesture(f.Hands[0]).ToString();

			if(stop)
				result.text= poseManager.didIWin(pose,otherPose).ToString();

			Transform modelHandContainer=getHandContainer(handOrientation);

			modelHandContainer.transform.rotation = (Quaternion)stream.ReceiveNext ();

            for (int i = 0; i< modelHandContainer.GetChild(0).childCount; i++) {
                Quaternion[] data = (Quaternion[])stream.ReceiveNext ();
    
                Transform finger = modelHandContainer.GetChild (0).GetChild (i);

                Transform bone1 = finger;
                Transform bone2 = bone1.GetChild (0);
                Transform bone3 = bone2.GetChild (0);

                bone1.rotation = data [0];
                bone2.rotation = data [1];
                bone3.rotation = data [2];
            }

			bool opponentReady=(bool)stream.ReceiveNext();

			if(opponentReady && IAmReady)
				go=true;

		}
	}

	Transform getHandContainer(string hand){

		if (hand == "Left") {
			rightHandModel.gameObject.SetActive (false);
			leftHandModel.SetActive (true);
			return leftHandModel.transform.FindChild ("HandContainer"); 
		}
		else {
			rightHandModel.SetActive (true);
			leftHandModel.SetActive (false);
			return rightHandModel.transform.FindChild ("HandContainer"); 
		}
	}
	/**
	 * Class is used store some extra details returned by leap hand
	 */
	public class LeapHandWrapper{

		public GameObject hand;
		public string orientation;
		public PoseManager.POSE pose;

		public LeapHandWrapper(GameObject hand,string orientation,PoseManager.POSE pose){
			this.hand=hand;
			this.orientation=orientation;
			this.pose=pose;
		}



	}

}